﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Pizzeria.Domain.Entities
{
    public class CategoryEntity
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage ="Nazwa jest wymagana")]
        [MinLength(2,ErrorMessage ="Nazwa musi mieć co najmniej 2 znaki")]
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public virtual ICollection<ProductEntity> Products { get; set; }

        public CategoryEntity()
        {
            Id = Guid.NewGuid();
            Products = new List<ProductEntity>();
        }
        public bool ImageToByte(IFormFile file)
        {
            if (file != null)
            {
                if (file.ContentType.Contains("image"))
                {

                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        Image = ms.GetBuffer();
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
