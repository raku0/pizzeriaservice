﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Pizzeria.Domain.Entities
{
    public class OrderEntity
    {
        [Key]
        public Guid Id { get; set; }
        public float TotalPrice
        {
            get
            {
                float price = 0;
                if (Products != null)
                {
                    foreach (ProductCountEntity item in Products)
                    {
                        if (item.Product != null)
                        {
                            price += item.Product.Price * item.Count;
                        }
                    }
                }
                return price;
            }
        }
        [Required]
        public bool IsDone { get; set; }
        public string Comment { get; set; }
        [Required]
        public bool IsOwnPickup { get; set; }
        public string Address { get; set; }
        [Required]
        public bool IsPaid { get; set; }
        [Required]
        public bool PackIt { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }
        public DateTime FinishDate { get; set; }
        public virtual ICollection<ProductCountEntity> Products { get; set; }

        public OrderEntity()
        {
            Id = Guid.NewGuid();
            Products = new List<ProductCountEntity>();
            IsDone = false;
            IsOwnPickup = true;
            IsPaid = false;
            PackIt = false;
        }
    }
}
