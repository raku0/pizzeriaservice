﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pizzeria.Domain.Entities
{
    public class ProductCountEntity
    {
        [Key]
        public Guid Id { get; set; }       
        [ForeignKey("ProductId")]
        public virtual Guid ProductId { get; set; }
        public virtual ProductEntity Product { get; set; }
        public int Count { get; set; }
        public string Comment { get; set; }       
        [ForeignKey("OrderId")]
        public virtual Guid OrderId { get; set; }
        public virtual OrderEntity Order { get; set; }

        public ProductCountEntity()
        {
            Id = Guid.NewGuid();
            Count = 1;
        }
    }
}
