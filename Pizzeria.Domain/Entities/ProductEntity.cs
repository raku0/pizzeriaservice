﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pizzeria.Domain.Entities
{
    public class ProductEntity
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Kategoria jest wymagana")]        
        public virtual CategoryEntity Category { get; set; }
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        [MinLength(3, ErrorMessage = "Minimum 3 znaki")]
        [MaxLength(50, ErrorMessage = "Maksimum 50 znaków")]
        public string Name { get; set; }
        public string SubName { get; set; }
        [Required(ErrorMessage = "Cena jest wymagana")]
        public float Price { get; set; }
        [Required(ErrorMessage = "Numer kasy jest wymagany")]
        public int CashRegisterNumber { get; set; }
        [Required]
        public bool IsBlocked { get; set; }
        [ForeignKey("CategoryId")]
        public Guid CategoryId { get; set; }
        public bool CheckSauce { get; set; }
        public virtual ICollection<ProductCountEntity> ProductCounts { get; set; }
        

        public ProductEntity()
        {
            Id = Guid.NewGuid();
            ProductCounts = new List<ProductCountEntity>();
            IsBlocked = false;
            CheckSauce = false;
        }
    }
}
