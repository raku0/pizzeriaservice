﻿using Microsoft.EntityFrameworkCore;
using Pizzeria.Domain.Entities;


namespace Pizzeria.Domain.PizzeriaContext
{
    public class PizzeriaDbContext : DbContext
    {
        public PizzeriaDbContext(DbContextOptions<PizzeriaDbContext> options) :
            base(options)
        { }

        public DbSet<OrderEntity> Orders { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<ProductCountEntity> ProductsCount { get; set; }
        public DbSet<CategoryEntity> ProductCategory { get; set; }
    }
}
