using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pizzeria.Domain.PizzeriaContext;
using Pizzeria.Utilities;
using Pizzeria.Utilities.Interfaces;
using System.IO;

namespace Pizzeria
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddDbContext<PizzeriaDbContext>(options => options.UseSqlServer("Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=" +
            Directory.GetCurrentDirectory() + "\\App_Data\\PizzeriaDb.mdf;" +
            "Initial Catalog=PizzeriaDb.mdf;Integrated Security=True;Connect Timeout=30;User Instance=False"),ServiceLifetime.Transient);
            services.AddTransient<IOrder, OrderService>();
            services.AddTransient<IProduct, ProductService>();
            services.AddTransient<ICategory, CategoryService>();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
