﻿using Microsoft.EntityFrameworkCore;
using Pizzeria.Domain.Entities;
using Pizzeria.Domain.PizzeriaContext;
using Pizzeria.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Utilities
{
    public class CategoryService : ICategory
    {
        private readonly PizzeriaDbContext DbContext;
        public CategoryService(PizzeriaDbContext db)
        {
            DbContext = db;
        }
        public Task<List<CategoryEntity>> GetAsync()
        {
            return DbContext.ProductCategory.ToListAsync();
        }
        public Task<bool> AddAsync(CategoryEntity category)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {                    
                    DbContext.ProductCategory.Add(category);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<bool> DeleteAsync(Guid id)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.ProductCategory.Remove(DbContext.ProductCategory.Find(id));
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<bool> UpdateAsync(CategoryEntity category)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.ProductCategory.Update(category);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<CategoryEntity> GetAsync(Guid id)
        {
            return Task<CategoryEntity>.Factory.StartNew(() =>
            {
                return DbContext.ProductCategory.Single(pro => pro.Id == id);
            });
        }
        public Task<bool> UpdateAsync(List<CategoryEntity> categorys)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.ProductCategory.UpdateRange(categorys);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
    }
}
