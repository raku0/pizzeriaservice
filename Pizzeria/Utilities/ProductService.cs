﻿using Microsoft.EntityFrameworkCore;
using Pizzeria.Domain.Entities;
using Pizzeria.Domain.PizzeriaContext;
using Pizzeria.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Utilities
{
    public class ProductService : IProduct
    {
        private readonly PizzeriaDbContext DbContext;

        public ProductService(PizzeriaDbContext db)
        {
            DbContext = db;
        }
        public Task<bool> AddAsync(ProductEntity product)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Products.Add(product);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<bool> DeleteAsync(Guid id)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Products.Remove(DbContext.Products.Find(id));
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<List<ProductEntity>> GetAsync()
        {
            return DbContext.Products.ToListAsync();
        }
        public Task<List<ProductEntity>> GetProductByCashNumberAsync(int cashNumber)
        {
            return Task<List<ProductEntity>>.Factory.StartNew(() =>
            {
                return DbContext.Products.Where(pro => pro.CashRegisterNumber == cashNumber).ToList();
            });
        }
        public Task<bool> UpdateAsync(ProductEntity model)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Products.Update(model);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<bool> UpdateAsync(List<ProductEntity> products)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Products.UpdateRange(products);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
        public Task<ProductEntity> GetAsync(Guid id)
        {
            return Task<ProductEntity>.Factory.StartNew(() =>
            {
                return DbContext.Products.Single(pro => pro.Id == id);
            });
        }
    }
}
