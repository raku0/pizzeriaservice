﻿using Microsoft.EntityFrameworkCore;
using Pizzeria.Domain.Entities;
using Pizzeria.Domain.PizzeriaContext;
using Pizzeria.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pizzeria.Utilities
{
    public class OrderService : IOrder
    {
        private readonly PizzeriaDbContext DbContext;

        public OrderService(PizzeriaDbContext db)
        {
            DbContext = db;
        }

        public Task<bool> AddAsync(OrderEntity order)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Orders.Add(order);
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }

        public Task<List<OrderEntity>> GetOrdersAsync()
        {
            return DbContext.Orders.Include(i=>i.Products).ThenInclude(e=>e.Product).ToListAsync();
        }

        public Task<OrderEntity> GetByGUIDAsync(Guid guid)
        {
            return Task<OrderEntity>.Factory.StartNew(() =>
            {
                return DbContext.Orders.Find(guid);
            });
        }

        public Task<bool> UpdateAsync(OrderEntity model)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                OrderEntity order = DbContext.Orders.Find(model.Id);
                if (order != null)
                {
                    DbContext.Orders.Update(model);
                    DbContext.SaveChanges();
                    return true;
                }
                return false;
            });
        }

        public Task<bool> DeleteAsync(Guid guid)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbContext.Orders.Remove(DbContext.Orders.Find(guid));
                    DbContext.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }
    }
}