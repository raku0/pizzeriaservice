﻿using Pizzeria.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pizzeria.Utilities.Interfaces
{
    public interface IOrder
    {
        public Task<List<OrderEntity>> GetOrdersAsync();
        public Task<OrderEntity> GetByGUIDAsync(Guid guid);
        public Task<bool> UpdateAsync(OrderEntity model);
        public Task<bool> DeleteAsync(Guid guid);
        public Task<bool> AddAsync(OrderEntity order);
    }
}
