﻿using Pizzeria.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pizzeria.Utilities.Interfaces
{
    public interface IProduct
    {
        public Task<List<ProductEntity>> GetProductByCashNumberAsync(int cashNumber);
        public Task<ProductEntity> GetAsync(Guid id);
        public Task<bool> AddAsync(ProductEntity product);
        public Task<bool> DeleteAsync(Guid id);
        public Task<List<ProductEntity>> GetAsync();
        public Task<bool> UpdateAsync(ProductEntity model);
        public Task<bool> UpdateAsync(List<ProductEntity> products);        
    }
}
