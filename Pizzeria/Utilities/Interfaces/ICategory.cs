﻿using Pizzeria.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.Utilities.Interfaces
{
    public interface ICategory
    {
        public Task<List<CategoryEntity>> GetAsync();
        public Task<CategoryEntity> GetAsync(Guid id);
        public Task<bool> AddAsync(CategoryEntity category);
        public Task<bool> DeleteAsync(Guid id);
        public Task<bool> UpdateAsync(CategoryEntity category);
        public Task<bool> UpdateAsync(List<CategoryEntity> categorys);
    }
}
