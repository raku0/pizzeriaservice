using Microsoft.EntityFrameworkCore;
using Pizzeria.Domain.Entities;
using Pizzeria.Domain.PizzeriaContext;
using Pizzeria.Utilities;
using Pizzeria.Utilities.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace PizzeriaTests
{
    public class ProductServiceTests
    {
        [Fact]
        public void TestProductMethods()
        {
            IProduct productService = (ProductService)GetInMemoryRepository("testproductadd", typeof(IProduct));
            ICategory categoryService = (CategoryService)GetInMemoryRepository("testproductaddd", typeof(ICategory));
            PrepareCategory(categoryService);
            Task<List<CategoryEntity>> categories = categoryService.GetAsync();
            categories.Wait();
            ProductEntity product = new ProductEntity()
            {
                Name = "product",
                Price = 2,
                CashRegisterNumber = 101,
                IsBlocked = false,
                Category = categories.Result[0]
            };
            Task<bool> result = productService.AddAsync(product);
            result.Wait();
            Assert.True(result.Result);
            product.Name = "updated";
            Task<bool> update = productService.UpdateAsync(product);
            update.Wait();
            Task<ProductEntity> productUpdated = productService.GetAsync(product.Id);
            productUpdated.Wait();
            Assert.Equal("updated", productUpdated.Result.Name);
            Task<List<ProductEntity>> productEntity = productService.GetProductByCashNumberAsync(101);
            productEntity.Wait();
            Assert.NotEmpty(productEntity.Result);
            Task<bool> resultDelete = productService.DeleteAsync(product.Id);
            resultDelete.Wait();
            Assert.True(resultDelete.Result);
        }
        [Fact]
        //no idea why have to run it alone or fail 
        public void TestCategoryPrepare()
        {
            ICategory categoryService = (CategoryService)GetInMemoryRepository("prepare", typeof(ICategory));
            PrepareCategory(categoryService);
            Task<List<CategoryEntity>> categories = categoryService.GetAsync();
            categories.Wait();
            Assert.Equal(2, categories.Result.Count);
        }
        [Fact]
        public void TestCategoryAdd()
        {
            ICategory categoryService = (CategoryService)GetInMemoryRepository("addCategory", typeof(ICategory));
            PrepareCategory(categoryService);
            CategoryEntity category = new CategoryEntity()
            {
                Name = "test"
            };
            Task<bool> addResult = categoryService.AddAsync(category);
            addResult.Wait();
            Assert.True(addResult.Result);
            Task<List<CategoryEntity>> categories = categoryService.GetAsync();
            categories.Wait();
            Assert.Equal(3, categories.Result.Count);
        }
        [Fact]
        public void TestCategoryUpdate()
        {
            ICategory categoryService = (CategoryService)GetInMemoryRepository("update", typeof(ICategory));
            CategoryEntity category = new CategoryEntity()
            {
                Name = "test"
            };
            Task<bool> add = categoryService.AddAsync(category);
            add.Wait();
            category.Name = "changed test";
            Task<bool> result = categoryService.UpdateAsync(category);
            result.Wait();
            Assert.True(result.Result);
            Task<CategoryEntity> categoryUpdated = categoryService.GetAsync(category.Id);
            categoryUpdated.Wait();
            Assert.Equal("changed test", category.Name);
        }

        private object GetInMemoryRepository<T>(string dbname, T typeService)
        {
            DbContextOptions<PizzeriaDbContext> options;
            var builder = new DbContextOptionsBuilder<PizzeriaDbContext>();
            builder.UseInMemoryDatabase(dbname);
            builder.EnableDetailedErrors(true);
            options = builder.Options;
            PizzeriaDbContext dbContext = new PizzeriaDbContext(options);
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
            if (typeService.Equals(typeof(ICategory)))
            {
                ICategory service = new CategoryService(dbContext);
                return service;
            }
            else if (typeService.Equals(typeof(IProduct)))
            {
                IProduct service = new ProductService(dbContext);
                return service;
            }
            return null;
        }
        private void PrepareCategory(ICategory service)
        {
            service.AddAsync(new CategoryEntity() { Name = "test category one" });
            service.AddAsync(new CategoryEntity() { Name = "test category two" });
        }
    }
}
